package com.example.bottomnavigation.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.bottomnavigation.Adapter.InfoWindowData;
import com.example.bottomnavigation.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Cap;
import com.google.android.gms.maps.model.CustomCap;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.maps.android.SphericalUtil;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static com.google.android.gms.maps.model.BitmapDescriptorFactory.*;

public class GmapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private static final int ROUND = 2;
    private GoogleMap mMap;


    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    private static final int COLOR_BLACK_ARGB = 0xff000000;
    private static final int POLYLINE_STROKE_WIDTH_PX = 12;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gmaps, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }







        /* List<Marker> lstMarcadores = new ArrayList<>();
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        Marker marker;
        marker = mMap.addMarker(new MarkerOptions().position(new LatLng(12.983379, 77.608976))
                .title("Shivaji Nagar")
                .snippet("Famous:Shoping"));
        lstMarcadores.add(marker);
        lstMarcadores.get(0).showInfoWindow();

        Marker marker2;
        marker2 = mMap.addMarker(new MarkerOptions().position(new LatLng(13.199163, 77.706140))
                .title("Kempegowda Int'l Airport")
                .snippet("Famous:Airport"));
        lstMarcadores.add(marker2);
        lstMarcadores.get(1).showInfoWindow();

        Marker marker3;
        marker3 = mMap.addMarker(new MarkerOptions().position(new LatLng(12.988009, 77.737388))
                .title("Whitefield")
                .snippet("Famous:Tech Parks"));
        lstMarcadores.add(marker3);
        lstMarcadores.get(2).showInfoWindow();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();


        builder.include(marker.getPosition());
        builder.include(marker2.getPosition());
        builder.include(marker3.getPosition());


        LatLngBounds bounds = builder.build();
        int padding = 200;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.moveCamera(cu);
        googleMap.animateCamera(cu);*/


       LatLng latLng = new LatLng(12.983379, 77.608976);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng)
                .title("Shivaji Nagar")
                .snippet("Thi place located 10 KM from Dairy Circle.")
                .icon(defaultMarker(HUE_RED));


        InfoWindowData info = new InfoWindowData();
        info.setImage("snowqualmie");
        info.setHotel("Hotel : excellent hotels available");
        info.setFood("Food : all types of restaurants available");
        info.setTransport("Reach the site by bus, car and train.");


        LatLng latLng1 = new LatLng(13.199163, 77.706140);

        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(latLng1)
                .title("Kempegowda ")
                .snippet("Kempegowda Int'l Airport")
                .icon(defaultMarker(HUE_RED));

        InfoWindowData info1 = new InfoWindowData();
        info1.setImage("snowqualmie");
        info1.setHotel("Hotel : excellent hotels available");
        info1.setFood("Food : all types of restaurants available");
        info1.setTransport("Reach the site by bus, car and train.");

        LatLng latLng2 = new LatLng(12.988009, 77.737388);

        MarkerOptions markerOptions2 = new MarkerOptions();
        markerOptions2.position(latLng2)
                .title("Whitefiled")
                .snippet("Whitefiled")
                .icon(defaultMarker(HUE_RED));

        InfoWindowData info2 = new InfoWindowData();
        info2.setImage("snowqualmie");
        info2.setHotel("Hotel : excellent hotels available");
        info2.setFood("Food : all types of restaurants available");
        info2.setTransport("Reach the site by bus, car and train.");


//        Polyline line = mMap.addPolyline(new PolylineOptions()
//                .add(new LatLng(12.983379, 77.608976), new LatLng(12.988009, 77.737388))
//                .width(5)
//                .color(Color.RED)
//                .jointType(ROUND)
//        );
//
//
//        Polyline line2 = mMap.addPolyline(new PolylineOptions()
//                .add(new LatLng(12.983379, 77.608976), new LatLng(13.199163, 77.706140))
//                .width(5)
//                .color(Color.RED)
//                .jointType(ROUND)
//                .endCap(new RoundCap())
//                .geodesic(true));

       /* List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(30), new Gap(10));
        PolylineOptions popt = new PolylineOptions().add(latLng).add(latLng1)
                .width(10).color(Color.MAGENTA).pattern(pattern)
                .geodesic(true);
        mMap.addPolyline(popt);*/


        CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(this);
        mMap.setInfoWindowAdapter(customInfoWindow);

        Marker m = mMap.addMarker(markerOptions);
        m.setTag(info);
        m.showInfoWindow();

        Marker m1 = mMap.addMarker(markerOptions1);
        m1.setTag(info);
        m1.showInfoWindow();

        Marker m2 = mMap.addMarker(markerOptions2);
        m2.setTag(info);
        m2.showInfoWindow();

      /*  LatLngBounds.Builder builder = new LatLngBounds.Builder();


        builder.include(markerOptions.getPosition());
        builder.include(markerOptions1.getPosition());
        builder.include(markerOptions2.getPosition());


        LatLngBounds bounds = builder.build();
        int padding = 200;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.moveCamera(cu);
        googleMap.animateCamera(cu);*/

       /* LatLng latLng1 = new LatLng(12.983379, 77.608976); // New York
        LatLng latLng2 = new LatLng(12.988009, 77.737388); // London

        Marker marker1 = mMap.addMarker(new MarkerOptions().position(latLng1).title("Start"));
        Marker marker2 = mMap.addMarker(new MarkerOptions().position(latLng2).title("End"));*/

        List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(30), new Gap(20));
        PolylineOptions popt = new PolylineOptions().add(latLng).add(latLng1)
                .width(10).color(Color.MAGENTA).pattern(pattern)
                .geodesic(true);
//        mMap.addPolyline(popt);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        builder.include(markerOptions.getPosition());
        builder.include(markerOptions1.getPosition());
        builder.include(markerOptions2.getPosition());

        LatLngBounds bounds = builder.build();
        int padding = 150; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.moveCamera(cu);
        mMap.animateCamera(cu);
        this.showCurvedPolyline(latLng,latLng1,latLng2, 0.5);
        this.showCurvedPolyline(latLng1,latLng2,latLng,0.5);
    }
    private void showCurvedPolyline (LatLng p1, LatLng p2,LatLng p3, double k) {
        //Calculate distance and heading between two points
        double d = SphericalUtil.computeDistanceBetween(p1,p2);
        double h = SphericalUtil.computeHeading(p1, p2);
        double j = SphericalUtil.computeHeading(p1, p2);

        //Midpoint position
        LatLng p = SphericalUtil.computeOffset(p1, d*0.5, h);

        //Apply some mathematics to calculate position of the circle center
        double x = (1-k*k)*d*0.5/(2*k);
        double r = (1+k*k)*d*0.5/(2*k);

        LatLng c = SphericalUtil.computeOffset(p, x, h + 90.0);

        //Polyline options
        PolylineOptions options = new PolylineOptions();
        List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(30), new Gap(20));

        //Calculate heading between circle center and two points
        double h1 = SphericalUtil.computeHeading(c, p1);
        double h2 = SphericalUtil.computeHeading(c, p2);
        double h3 = SphericalUtil.computeHeading(c, p3);

        //Calculate positions of points on circle border and add them to polyline options
        int numpoints = 100;
        double step = (h2 -h1) / numpoints;
        double step1 = (h3 -h2) / numpoints;

        for (int i=0; i < numpoints; i++) {
            LatLng pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
            options.add(pi);
        }

        //Draw polyline
        mMap.addPolyline(options.width(10).color(Color.MAGENTA).geodesic(false).pattern(pattern));
    }





  /*  public void stylePolyline() {
        String type = "";
        Polyline line2 = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(12.983379, 77.608976), new LatLng(13.199163, 77.706140)));


        if (line2.getTag() != null) {
            type = line2.getTag().toString();
        }

        switch (type) {
            case "A":

                line2.setStartCap(
                        new CustomCap(
                                BitmapDescriptorFactory.fromResource(R.drawable.ic_keyboard_arrow_right_black_24dp), 10));
                break;
            case "B":
                line2.setStartCap(new RoundCap());
                break;
        }

        line2.setEndCap(new RoundCap());
        line2.setWidth(POLYLINE_STROKE_WIDTH_PX);
        line2.setColor(COLOR_BLACK_ARGB);
        line2.setJointType(JointType.ROUND);
    }*/


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getContext())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((Activity) getContext(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getContext(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {


        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }


        LatLng latLng3 = new LatLng(location.getLatitude(), location.getLongitude());
        Log.e("Getting Current Locatin", location.getLatitude() + "");
        Log.e("Getting  Location", location.getLongitude() + "");

        MarkerOptions marker = new MarkerOptions();
        marker.position(latLng3)
                .title("Current Position")
                .icon(defaultMarker(HUE_BLUE));


        InfoWindowData info2 = new InfoWindowData();
       /* info2.setImage("snowqualmie");
        info2.setHotel("Hotel : excellent hotels available");
       info2.setFood("Food : all types of restaurants available");*/
//      info2.setTransport("Reach the site by bus, car and train.");
        info2.setInfo("I am Here");
        info2.setCurrentLocation(true);


        CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(this);
        mMap.setInfoWindowAdapter(customInfoWindow);

        /*Marker m = mMap.addMarker(marker);
        m.setTag(info);
        m.showInfoWindow();*/

        mCurrLocationMarker = mMap.addMarker(marker);
        mCurrLocationMarker.setTag(info2);
        mCurrLocationMarker.showInfoWindow();


    }


}

