package com.example.bottomnavigation.Fragments;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bottomnavigation.Adapter.InfoWindowData;
import com.example.bottomnavigation.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {
    public static String PACKAGE_NAME;
    private GmapFragment gmapFragment;

    public CustomInfoWindowGoogleMap(GmapFragment gmapFragment) {
        this.gmapFragment = gmapFragment;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view;
        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();
        if (infoWindowData.isCurrentLocation()) {
            view = (gmapFragment).getLayoutInflater().inflate(R.layout.currnet_info, null);

        } else {
            view = (gmapFragment).getLayoutInflater().inflate(R.layout.info_window, null);



//        View view = (gmapFragment).getLayoutInflater().inflate(R.layout.info_window, null);


        TextView name_tv = view.findViewById(R.id.name);
        TextView details_tv = view.findViewById(R.id.details);
        ImageView img = view.findViewById(R.id.pic);

        TextView hotel_tv = view.findViewById(R.id.hotels);
        TextView food_tv = view.findViewById(R.id.food);
        TextView transport_tv = view.findViewById(R.id.transport);

        name_tv.setText(marker.getTitle());
        details_tv.setText(marker.getSnippet());

//        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();


       /* int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());*/

        Picasso.with(gmapFragment.getContext()).load("http://i.imgur.com/DvpvklR.png").error(R.mipmap.ic_launcher).into(img);


        hotel_tv.setText(infoWindowData.getHotel());
        food_tv.setText(infoWindowData.getFood());
        transport_tv.setText(infoWindowData.getTransport());
    }

        return view;
    }



}
