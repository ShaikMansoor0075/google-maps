package com.example.bottomnavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toolbar;

import com.example.bottomnavigation.Fragments.FavoriteFragments;
import com.example.bottomnavigation.Fragments.GmapFragment;
import com.example.bottomnavigation.Fragments.HomeFragments;
import com.example.bottomnavigation.Fragments.SearchFragments;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomnav = findViewById(R.id.bottom_navigation);
        bottomnav.setOnNavigationItemSelectedListener(navListner);
        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeFragments()).commit();

            bottomnav.setSelectedItemId(R.id.nav_home);
        }




    }




    private BottomNavigationView.OnNavigationItemSelectedListener navListner =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()) {
                        case R.id.nav_home:
                            selectedFragment = new HomeFragments();
                            break;
                        case R.id.nav_favorites:
                            selectedFragment = new FavoriteFragments();
                            break;
                        case R.id.nav_search:
                            selectedFragment = new SearchFragments();
                            break;
                        case R.id.nav_map:
                            selectedFragment = new GmapFragment();
                            break;
                    }
                  getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment)
                            .addToBackStack("tag").commit();
                    return true;
                }
            };
}

